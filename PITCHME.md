## Strongly Anharmonic Solids
#### From Screening to Accurate Thermal Conductivities

<br/>
Florian Knoop<br/>
@size[.75em](Thomas Purcell, Matthias Scheffler, Christian Carbogno)

@snap[north-west span-40]
![](assets/img/FHIlogo.png)
@snapend

@snap[south span-100]
@size[.75em](DPG Frühjahrstagung Regensburg – MM 37.9)
@snapend

`$ \renewcommand{\b}[1]{\boldsymbol{#1}}$`

<!--
@snap[north span-100]
### Thermal Conductivity `$ \kappa $`
@snapend

@ul[west span-55](false)
- **Thermal management**
- Thermal barrier coatings
- Thermoelectrics
@ulend

@img[east span-40 shadow](assets/img/heatsink.jpg)

@snap[north span-100]
### Thermal Conductivity `$ \kappa $`
@snapend

@ul[west span-55](false)
- Thermal management
- **Thermal barrier coatings**
- Thermoelectrics
@ulend

@img[east span-40 shadow](assets/img/turbine.jpg)

-->

---
@snap[north span-100]
### Thermal Conductivity `$ \kappa $`
@snapend

@ul[west span-55](false)
- Interesting Physics
- Thermal management
- Thermal barrier coatings
- **Thermoelectrics**
  + Efficiency  `$~\propto \kappa^{-1}$`
@ulend

@img[east span-40 shadow](assets/img/thermoel.jpg)

@snap[south span-100 fragment]
**Much Less** than 1000 materials investigated in detail
<br/><br/>
@snapend

<!--
@snap[north span-100]
### Why less than 1000 materials?
@snapend

@snap[north span-100]
<br/><br/><br/>
**Involved Workflow** (# DFT Calculations):
@ul
- Relax Systems (10)
- Compute Phonons (10)
- Model Anharmonicity (>> 100)
- Lattice Expansion?  (>> 100)
@ulend
@snapend
-->

---
@snap[north span-100]
### Nuclear Dynamics
@snapend

@ul[east span-40](false)
- Free Energy
- Heat Capacity
- Thermal Conductivity: **Infinite**
@ulend

@img[west span-45 shadow](assets/sketches/PES2.png)<br/>

@snap[south span-100]
**Harmonic** Model: **Phonons**
<br/>
@snapend

---
@snap[north span-100]
### Nuclear Dynamics
@snapend

@ul[east span-40](false)
- Lattice Expansion
- Electron-Phonon Coupling
- **Thermal Conductivity**
@ulend

@img[west span-45 shadow](assets/sketches/PES3.png)<br/>

@snap[south span-100]
Many properties determined by **anharmonicity**
<br/>
@snapend

---
@snap[north span-100]
### The Highthroughput Way
@snapend

@img[midpoint span-75](assets/flowcharts/highthroughput.png)

@snap[south span-100]
Screen for **target property** with **standardized** method
@snapend

---
@snap[north span-100]
### **Our** Highthroughput Way
@snapend

@img[midpoint span-75](assets/flowcharts/my_highthroughput_2.png)

@snap[south span-100]
**Identify** interesting materials and **investigate further**
@snapend

---
@snap[north span-100]
### **Our** Highthroughput Way
@snapend

@img[midpoint span-75](assets/flowcharts/my_highthroughput_2.png)

@snap[south span-100]
**Our Implementation:** <a href="gitlab.com/flokno/hilde">**gitlab.com/flokno/hilde**</a>
@snapend

---
@snap[north span-100]
### Anharmonicity Quantification
@snapend

<br/>
`$
\begin{align}
    U_\text{pot} (\b R) &= U^{(2)} + U' \\ \\
    \b F                &= \b F^\text{ha} + \b F' ~~~~~~~\,
\end{align}
$`
<br/>
<p style='line-height:.2'>
<br/>
<p/>
<dummy class='fragment'>
`$
\begin{align}
    \left\langle \left( \b F' \right)^2 \right\rangle &=
    \left\langle \left( \b F - \b F^\text{ha} \right)^2 \right\rangle \\ \\
    &\propto 1 - r^2 (\b F , \b F^\text{ha})
\end{align}
$`
<dummy/>

@snap[south span-100 fragment]
$r^2$ : **Coefficient of Determination**
@snapend

---
@snap[north span-100]
### Anharmonicity Quantification
@snapend

@snap[north-west span-100]
<br/><br/>
<br/>
**Coefficient of Determination**:
<!--
`$
\begin{align}
    r^2 \left( \b F^\text{DFT},~ \b F^\text{ha} \right)
    = 1 - \frac{(\b F^\text{DFT} - \b F^\text{ha})^2}{(\b F^\text{DFT} - \bar{\b F} ^\text{DFT})^2}
    \vphantom{\sum_{\b q}\frac{1}{\omega_{\b q}}}
\end{align}
$`
-->
@snapend
<br/>
@ul[fragment](false)
- `$r^2 = 1~~$`: &nbsp; perfectly **harmonic**
- `$r^2 = 0~~$`: &nbsp; maximally **anharmonic**
@ulend

<div class='fragment' align='left'>
<br/>
Ensemble average
`$\small \left\langle \left( \b F - \b F^\text{ha} \right)^2 \right\rangle~~$` &nbsp; :
<div/>

@snap[south span-100]
@ul[fragment](false)
- `$\b F \equiv \b F^\text{DFT}$`
- Molecular Dynamics
- Monte Carlo Sampling
@ulend
<br/>
@snapend

---
@snap[north span-100]
### @size[.8em](`$r^2~$`) from Molecular Dynamics
@snapend

@img[west span-40 bshadow fragment](assets/r_squared/MgO/dft_vs_ha_xyz_mgo.png)
@img[east span-40 bshadow fragment](assets/r_squared/CsPbI3/dft_vs_ha_xyz.png)

@snap[south span-100 fragment]
Do we need to perform MD simulations to get `$~r^2~$`?
@snapend

<!--
@snap[north span-100]
### @size[.8em](`$r^2~$`) from Molecular Dynamics
@snapend

@img[west span-40 bshadow](assets/r_squared/MgO/dft_vs_ha_xyz.png)
@img[east span-40 bshadow](assets/r_squared/CsPbI3/dft_vs_ha_xyz.png)

@snap[south span-100]
**No:** Estimate **average thermal displacement**
@snapend
-->

---
@snap[north span-100]
### @size[.8em]($r^2$) from Statistical Sampling
@snapend

@img[west span-40 bshadow](assets/r_squared/MgO/dft_vs_ha_xyz_mgo_sampling.png)
@img[east span-40 bshadow](assets/r_squared/CsPbI3/dft_vs_ha_xyz_sampling.png)

<!--
@snap[south span-100 ]
`$R^2$` obtained from Statistical Sampling
@snapend
-->

@snap[south span-100]
@size[.7em](Do this with only **one sample**: M. Zacharias, F. Giustino, _PRB_ **94**, 075125 (2016&#41;)
@snapend



---
@snap[north span-100]
### Screening Perovskites
@snapend

@snap[west span-45 fragment]
**Test Set**:
@ul[](false)
- **315** Materials
  + 165 cubic
  + 26 Wurtzite
  + 20 tetragonal
  + 96 orthorombic
+ 109 **Perovskites**
  - 36 Oxides
  - 54 Halides
@ulend
@snapend

@img[east bshadow span-45 fragment](assets/anharmonicity/histogram.png)

@snap[south-west]
@size[.6em](A. van Roekeghem _et al._, _PRX_ **6**, 041061 (2016&#41;)
@snapend

@img[east span-45 fragment](assets/anharmonicity/histogram_annotated.png)


---
@snap[north span-100]
### CsPbI`$_3$`
@snapend

@snap[north-west]
<br/><br/><br/>
Powerful host material
@ul[](false)
- **< 1 W/mK** in **orthorombic** Phase for T < 300K  [1]
- **Cubic** phase: Efficient Photovoltaics [2]
@ulend
@snapend

@snap[south span-100 fragment]
Problem: **`$~\alpha$`-Cubic** CsPbI`$_3$` **stable above ~600K**
<br/><br/><br/><br/>
@snapend

@snap[south-east]
<p style='line-height:.9'>
<font size=5>
[1] Kovalsky _et al._, *J. Chem. Phys. C* **121**, 3228 (2017&#41; <br/>
[2] Swarnkar _et al._, *Science* **354**, 92 (2016&#41;
<font/><p/>
@snapend

@snap[midpoint span-55 fragment]
<br/><br/>
@img[shadow](assets/CsPbI3/bs_000.png)
@snapend

@snap[midpoint span-55 fragment]
<br/><br/>
@img[](assets/CsPbI3/bs_600.png)
@snapend


---
@snap[north span-100]
### _Ab initio_ Green-Kubo
@snapend
<br/> 
`$
\begin{align}
    \renewcommand{\b}[1]{\boldsymbol{#1}}
    \kappa &\propto
    \int \mathrm{d} \tau ~ \left\langle \b J \, \b J \right\rangle (\tau)
\end{align}
$`
<br/> <br/>
`$
\begin{align}
    \b J (t) &= \sum_i \sigma_i \, \dot{\b R}_i (t)
\end{align}
$`
<br/>
<br/>

@snap[south span-100 fragment]
**No approximations** to potential energy surface
<br/><br/><br/>
@snapend

@snap[north-east span-20]
@img[](assets/logos/FHIaims.png)
<br/>
<br/>
@snapend


@snap[south-east]
@size[.7em](C. Carbogno, R. Ramprasad, M. Scheffler, *PRL* **118**, 175901 (2017&#41;)
@snapend

---
@snap[north span-100]
### CsPbI`$_3$` -- Thermal Conductivity
@snapend

@snap[midpoint span-75]
<br/>
@img[shadow](assets/kappa/kappa.png)
@snapend

@snap[south span-100 fragment]
Confirm **ultralow thermal conductivity** < 1 W / mK
<br/>
@snapend

@snap[midpoint span-75 fragment]
<br/>
@img[](assets/kappa/kappa_twochannel.png)
@snapend

---
@snap[north span-100]
### Summary & Outlook
@snapend

@snap[midpoint span-100]
@ul(false)
<font size=6>
- Screen for **anharmonic** materials
- Compute **thermal conductivities**
  - <a href="gitlab.com/flokno/hilde">**gitlab.com/flokno/hilde**</a>
  <br/>
- Ongoing **development** of Green Kubo method
- Ongoing investiation of **CsPbI`$_3$`** and further compounds
- Produce (Big) Data
<font/>
@ulend
@snapend

@snap[south-east fragment span-25]
@img[shadow](assets/tom.png)
@snapend

@snap[south fragment span-100]
**Thank You for your attention!**
@snapend
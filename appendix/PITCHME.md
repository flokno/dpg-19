
@snap[north span-100]
### CsPbI`$_3$` -- Thermal Conductivity
@snapend

@img[midpoint span-60 shadow](assets/kappa/kappa.png)

@snap[south]
**Green-Kubo coming soon!**
@snap

---
@snap[north span-100]
### Thermal Conductivity vs. Anharmonicity
@snapend

@img[midpoint span-60 shadow](assets/anharmonicity/3_gradient.png)

@snap[south]
**hello**
@snap


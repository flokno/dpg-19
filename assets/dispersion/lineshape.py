import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
from matplotlib.colors import LogNorm, to_hex, LinearSegmentedColormap

purple = "#bf0040ff"
# green = to_hex((0,125/256,107/256))

cm =  LinearSegmentedColormap.from_list(name='purples',
                                        colors=['white', 'grey'], N=250)

# open the sqe file
f = h5.File('outfile.sqe.hdf5','r')

# open the dispersions
d = h5.File('outfile.dispersion_relations.hdf5', 'r')
# get axes and intensity
x = np.array(f.get('q_values'))
qmax1 = x.max()
y = np.array(f.get('energy_values'))
gz = np.array(f.get('intensity'))

x2 = np.array(d.get('q_values'))
qmax2 = x2.max()
y2 = np.array(d.get('frequencies'))
# add a little bit so that the logscale does not go nuts
gz=gz+1E-2
# for plotting, turn the axes into 2d arrays
gx, gy = np.meshgrid(x,y)
# x-ticks
xt = np.array(f.get('q_ticks'))
# labels for the x-ticks
xl = [attr.decode('utf-8') for attr in f.attrs.get('q_tick_labels').split()]
for ii, let in enumerate(xl):
    if 'G' in let:
        xl[ii] = '$\Gamma$'
# label for y-axis
energy_unit = f.attrs.get('energy_unit').decode('utf-8')
yl = f"Energy\n[{energy_unit}]"

# set the limits of the plot to the limits of the data
fig, ax = plt.subplots()
ax.axis([x.min(), x.max(), y.min(), 16])
ax.set_xticks(xt)
ax.set_xticklabels(xl, size=25)
# ax.set_yticks(size=15)
ax.set_yticklabels(['' for _ in ax.get_yticklabels()])
plt.ylabel('$\omega_{\\bf{q}}$', size=25, rotation=0, labelpad=35)
# plt.title('Si diamond', size=20)
plt.tight_layout()
ax.plot(x2*qmax1/qmax2, y2, lw=2, color='k')#'crimson')
plt.savefig('dispersion_si.png', bbox_inches='tight')
ax = plt.gca()
ax.lines = []
# plt.plot(x2*qmax1/qmax2, y2, '--', lw=1, color='k')
plt.pcolormesh(gx, gy,
               gz, norm=LogNorm(vmin=gz.min(), vmax=.5*gz.max()),
               cmap=cm, #'PuRd',
               rasterized=True)
plt.ylabel('$\Sigma ({\\bf q}, \omega)$ ', size=25, rotation=0, labelpad=35)
plt.savefig('linewidth_si.png', bbox_inches='tight')

